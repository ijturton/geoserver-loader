package com.ianturton.loadpostgis;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.postgis.PostgisNGDataStoreFactory;
import org.geotools.geometry.jts.ReferencedEnvelope;

import it.geosolutions.geoserver.rest.GeoServerRESTPublisher;
import it.geosolutions.geoserver.rest.GeoServerRESTReader;
import it.geosolutions.geoserver.rest.encoder.GSLayerEncoder;
import it.geosolutions.geoserver.rest.encoder.GSLayerGroupEncoder23;
import it.geosolutions.geoserver.rest.encoder.datastore.GSPostGISDatastoreEncoder;
import it.geosolutions.geoserver.rest.encoder.feature.GSFeatureTypeEncoder;
import it.geosolutions.geoserver.rest.manager.GeoServerRESTStoreManager;

/**
 * Read a postgis schema and add all the tables to geoserver.
 * 
 * @author ian
 *
 */
public class Loader {
  private static final String DATABASEARG = "database";
  private static final String DATABASESHORT = "d";
  private static final String GPASSSHORT = "g";
  private static final String GPASSWDARG = "gpasswd";
  private static final String HELPARG = "help";
  private static final String HELPSHORT = "h";
  private static final String HOSTARG = "host";
  private static final String HOSTSHORT = null;
  private static final String LAYERSARG = "layers";
  private static final String LAYERSSHORT = "l";
  private static final String PASSWDARG = "passwd";
  private static final String PASSWDSHORT = "p";
  private static final String PORTARG = "port";
  private static final String PORTSHORT = null;
  private static final String REVERSEARG = "reverse";
  private static final String REVERSESHORT = "r";
  private static final String SCHEMAARG = "schema";
  private static final String SCHEMASHORT = "s";
  private static final String STYLESARG = "styles";
  private static final String STYLESSHORT = null;
  private static final String USERARG = "user";
  private static final String USERSHORT = "u";
  private static final String GURLSHORT = null;
  private static final String GURLWDARG = "url";
  private static final String WORKSHORT = "w";
  private static final String WORKARG = "workspace";
  private static final String LGRNSHORT = null;
  private static final String LGRNARG = "groupname";
  private static final String LGRWSHORT = null;
  private static final String LGRWARG = "groupinworkspace";
  private DataStore datastore;

  private GeoServerRESTStoreManager manager;
  private GeoServerRESTPublisher publisher;
  private GeoServerRESTReader reader;
  private String resturl = "http://localhost:8080/geoserver";

  final private String RESTUSER = "admin";

  private String workspace;

  public static void main(String[] args) throws Exception {
    CommandLineParser parser = new DefaultParser();
    Options options = new Options();
    Option user = new Option(USERSHORT, USERARG, true, "PostGIS Username, default username");
    options.addOption(user);
    Option password = new Option(PASSWDSHORT, PASSWDARG, true, "PostGIS Password, if missing look in .pgpass");
    options.addOption(password);
    Option host = new Option(HOSTSHORT, HOSTARG, true, "PostGIS host, default localhost");
    options.addOption(host);
    Option port = new Option(PORTSHORT, PORTARG, true, "PostGIS Port, default 5432");
    options.addOption(port);
    Option database = new Option(DATABASESHORT, DATABASEARG, true, "PostGIS database");
    options.addOption(database);
    Option schema = new Option(SCHEMASHORT, SCHEMAARG, true, "PostGIS Schema");
    options.addOption(schema);
    Option gPassword = new Option(GPASSSHORT, GPASSWDARG, true, "GeoServer Admin Password. default geoserver");
    options.addOption(gPassword);
    Option gurl = new Option(GURLSHORT, GURLWDARG, true, "GeoServer url. default http://localhost:8080/geoserver");
    options.addOption(gurl);
    Option rev = new Option(REVERSESHORT, REVERSEARG, false, "Reverse layers in group");
    options.addOption(rev);
    Option layers = new Option(LAYERSSHORT, LAYERSARG, true,
        "List of layers to order layer group, default to alphabetical order");
    options.addOption(layers);
    Option styles = new Option(STYLESSHORT, STYLESARG, true, "Directory of SLD files, default cwd");
    options.addOption(styles);
    Option work = new Option(WORKSHORT, WORKARG, true, "Workspace name, default schema name");
    options.addOption(work);
    Option lgrpName = new Option(LGRNSHORT, LGRNARG, true, "LayerGroup name, default schema name");
    options.addOption(lgrpName);
    Option lgrpWorkspace = new Option(LGRWSHORT, LGRWARG, false, "Store layergroup in workspace");
    options.addOption(lgrpWorkspace);
    options.addOption(new Option(HELPSHORT, HELPARG, false, "Get help"));
    CommandLine a = parser.parse(options, args);
    boolean fail = false;

    if (a.hasOption(HELPARG)) {
      print_help(options);
    }
    String geoserverPassword = "geoserver";
    if (a.hasOption(GPASSWDARG)) {
      geoserverPassword = a.getOptionValue(GPASSWDARG);
    }
    String geoserverUrl = "";
    if (a.hasOption(GURLWDARG)) {
      geoserverUrl = a.getOptionValue(GURLWDARG);
    }
    String host2 = "localhost";
    if (a.hasOption(HOSTARG)) {
      host2 = a.getOptionValue(HOSTARG);
    }
    String port2 = "5432";
    if (a.hasOption(PORTARG)) {
      port2 = a.getOptionValue(PORTARG);
    }
    String username = "";
    if (a.hasOption(USERARG)) {
      username = a.getOptionValue(USERARG);
    } else {
      username = System.getProperty("user.name");
    }
    String database2 = username;
    if (a.hasOption(DATABASEARG)) {
      database2 = a.getOptionValue(DATABASEARG);
    } else {
      System.err.println("Database is required");
      fail = true;
    }
    String password2 = null;
    if (a.hasOption(PASSWDARG)) {
      password2 = a.getOptionValue(PASSWDARG);
    }
    String schema2 = "";
    if (a.hasOption(SCHEMAARG)) {
      schema2 = a.getOptionValue(SCHEMAARG);
    } else {
      System.err.println("Schema is required");
      fail = true;
    }
    String worksp = schema2;
    if (a.hasOption(WORKARG)) {
      worksp = a.getOptionValue(WORKARG);
    }
    String groupName = schema2;
    if (a.hasOption(LGRNARG)) {
      groupName = a.getOptionValue(LGRNARG);
    }
    boolean inWorkspace = a.hasOption(LGRWARG);
    File styleDir = new File(".");
    if (a.hasOption(STYLESARG)) {
      styleDir = new File(a.getOptionValue(STYLESARG));
    }
    File layersFile = null;
    if (a.hasOption(LAYERSARG)) {
      layersFile = new File(a.getOptionValue(LAYERSARG));
    }
    boolean reverse = false;
    if (a.hasOption(REVERSEARG)) {
      reverse = true;
    }
    if (fail) {
      print_help(options);
    }
    Loader l = new Loader(geoserverPassword, geoserverUrl);
    l.checkAndCreateWorkspace(worksp);
    l.getDatabase(host2, port2, username, password2, database2, schema2);
    l.createDatastore(host2, port2, username, password2, database2, schema2);
    List<String> tables = l.getTables();
    for (String table : tables) {
      System.out.println("Publishing " + table);
      // add style
      l.createStyle(table, styleDir);
      l.publishTable(table);

    }

    List<String> layersList = new ArrayList<>();
    if (layersFile != null && layersFile.exists()) {
      BufferedReader breader = new BufferedReader(new FileReader(layersFile));
      String line = "";
      while ((line = breader.readLine()) != null) {
        layersList.add(line);
      }
      breader.close();

    } else {
      for (String table : tables) {
        layersList.add(table);
      }
    }

    // bah guessed wrong on direction
    if (reverse) {
      Collections.reverse(layersList);
    }
    l.createGroup(layersList, groupName, inWorkspace);
  }

  /**
   * @param options
   */
  public static void print_help(Options options) {
    HelpFormatter formatter = new HelpFormatter();
    formatter.printHelp("Loader", options);
    System.exit(1);
  }

  public Loader(String geoserverPassword, String url) throws MalformedURLException {
    if (url != null && !url.isEmpty()) {
      resturl = url;
    }
    reader = new GeoServerRESTReader(resturl, RESTUSER, geoserverPassword);
    if (!reader.existGeoserver()) {
      System.err.println("Can't connect to GeoServer at " + resturl);
      System.exit(1);
    }
    publisher = new GeoServerRESTPublisher(resturl, RESTUSER, geoserverPassword);
    manager = new GeoServerRESTStoreManager(new URL(resturl), RESTUSER, geoserverPassword);
  }

  boolean checkAndCreateWorkspace(String workspaceName) throws URISyntaxException {
    boolean ret = false;
    ret = reader.existsWorkspace(workspaceName, false);
    workspace = workspaceName;
    if (!ret) {
      ret = publisher.createWorkspace(workspaceName, new URI("http://ianturton.com/" + workspaceName));
    }
    return ret;
  }

  private void createDatastore(String host, String port, String username, String password, String database,
      String schema) {
    if (reader.existsDatastore(workspace, schema)) {
      publisher.removeDatastore(workspace, schema, true);
    }
    it.geosolutions.geoserver.rest.encoder.datastore.GSPostGISDatastoreEncoder encoder = new GSPostGISDatastoreEncoder(
        schema);
    encoder.setDatabase(database);
    encoder.setDatabaseType("postgis");
    encoder.setHost(host);
    encoder.setPort(Integer.parseInt(port));
    encoder.setSchema(schema);
    encoder.setUser(username);
    if (password == null) {
      password = getPassword(username, host, port, database);
    }
    encoder.setPassword(password);
    manager.create(workspace, encoder);

  }

  private void createGroup(List<String> layers, String name, boolean inWorkspace) {
    if (inWorkspace) {
      if (reader.existsLayerGroup(workspace, name)) {
        publisher.removeLayerGroup(workspace, name);
      }
      GSLayerGroupEncoder23 enc = new GSLayerGroupEncoder23();
      for (String layer : layers) {

        enc.addLayer(workspace + ":" + layer, layer);
      }
      publisher.createLayerGroup(workspace, name, enc);
    } else {
      if (reader.existsLayerGroup(null,name)) {
        publisher.removeLayerGroup(name);
      }
      GSLayerGroupEncoder23 enc = new GSLayerGroupEncoder23();
      for (String layer : layers) {

        enc.addLayer(workspace + ":" + layer, layer);
      }
      publisher.createLayerGroup(name, enc);
      
    }
  }

  private void createStyle(String table, File styleDir) {
    // We always search for name + "." otherwise rail matches railstations etc.
    File styleFile = null;
    for (File file : styleDir.listFiles()) {
      if (file.getName().toLowerCase().startsWith(table + ".")) {
        styleFile = file;
        break;
      }
    }
    if (styleFile == null) {
      // try with out an s on the end of the layername
      for (File file : styleDir.listFiles()) {
        if (file.getName().toLowerCase().startsWith(table.substring(0, table.length() - 2) + ".")) {
          styleFile = file;
          break;
        }
      }

    }
    if (styleFile != null) {
      if (reader.existsStyle(table)) {
        publisher.updateStyle(styleFile, table);
      } else {
        publisher.publishStyle(styleFile, table);
      }
    } else {
      System.err.println("no style found for " + table);
    }
  }

  private void getDatabase(String host, String port, String username, String password, String database, String schema) {
    if (password == null) {
      password = getPassword(username, host, port, database);
    }
    Map<String, Object> params = new HashMap<>();
    params.put(PostgisNGDataStoreFactory.DBTYPE.key, PostgisNGDataStoreFactory.DBTYPE.sample);
    params.put(PostgisNGDataStoreFactory.HOST.key, host);
    params.put(PostgisNGDataStoreFactory.PORT.key, port);
    params.put(PostgisNGDataStoreFactory.USER.key, username);
    params.put(PostgisNGDataStoreFactory.PASSWD.key, password);
    params.put(PostgisNGDataStoreFactory.DATABASE.key, database);
    params.put(PostgisNGDataStoreFactory.SCHEMA.key, schema);

    try {
      datastore = DataStoreFinder.getDataStore(params);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  public String getPassword(String user, String host, String port, String database) {
    String password = "";
    String home = System.getProperty("user.home");
    File file = new File(home, ".pgpass");
    if (!file.exists()) {
      return password;
    }
    try {
      BufferedReader breader = new BufferedReader(new FileReader(file));
      if (host.equalsIgnoreCase("127.0.0.1")) {
        host = "localhost";
      }
      String line = "";
      while ((line = breader.readLine()) != null) {
        String[] fields = line.split(":");
        if (fields[0].equalsIgnoreCase(host) && fields[1].equalsIgnoreCase(port) && fields[3].equalsIgnoreCase(user)) {
          if (fields[2].equalsIgnoreCase("*") || fields[2].equalsIgnoreCase(database)) {
            return fields[4];
          }
        }
      }
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return password;
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
      return password;
    }
    return password;
  }

  private List<String> getTables() {
    String[] names = {};
    try {
      names = datastore.getTypeNames();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    ArrayList<String> nameList = new ArrayList<String>();
    nameList.addAll(Arrays.asList(names));
    return nameList;
  }

  private void publishTable(String table) {
    if (reader.existsLayer(workspace, table)) {
      publisher.removeLayer(workspace, table);
    }
    GSLayerEncoder le = new GSLayerEncoder();
    le.addStyle(table);
    le.setAdvertised(false);

    GSFeatureTypeEncoder fte = new GSFeatureTypeEncoder();

    fte.setName(table);
    fte.setNativeCRS("EPSG:27700");
    fte.setEnabled(true);

    try {
      ReferencedEnvelope bounds = datastore.getFeatureSource(table).getBounds();
      fte.setNativeBoundingBox(bounds.getMinX(), bounds.getMinY(), bounds.getMaxX(), bounds.getMaxY(), "EPSG:27700");

    } catch (IOException e) {
      e.printStackTrace();
    }

    boolean result = publisher.publishDBLayer(workspace, "osopen_zoomstack", fte, le);
    if (!result) {
      System.err.println("Failed to publish " + table);
    }
  }
}
