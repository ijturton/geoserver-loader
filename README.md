# A Data Loader for GeoServer  

As I discussed in [my recent blog post](https://blog.ianturton.com/geoserver/2018/09/11/OS-Zoomstack.html) I got 
bored of uploading PostGIS tables to GeoServer and finally wrote a tool to automate it. 

If it's useful to you please make use of it.

Should be a simple matter of [downloading the jar file](https://gitlab.com/ijturton/geoserver-loader/blob/master/releases/loadpostgis-1.0.0.jar) and running 

    java -jar loadpostgis-1.0.0.jar
    
this will produce the help message:

        Database is required
        Schema is required
        usage: Loader
         -d,--database <arg>     PostGIS database
         -g,--gpasswd <arg>      GeoServer Admin Password. default geoserver
            --groupinworkspace   Store layergroup in workspace
            --groupname <arg>    LayerGroup name, default schema name
         -h,--help               Get help
            --host <arg>         PostGIS host, default localhost
         -l,--layers <arg>       List of layers to order layer group, default to
                                 alphabetical order
         -p,--passwd <arg>       PostGIS Password, if missing look in .pgpass
            --port <arg>         PostGIS Port, default 5432
         -r,--reverse            Reverse layers in group
         -s,--schema <arg>       PostGIS Schema
            --styles <arg>       Directory of SLD files, default cwd
         -u,--user <arg>         PostGIS Username, default username
            --url <arg>          GeoServer url. default
                                 http://localhost:8080/geoserver
         -w,--workspace <arg>    Workspace name, default schema name

You need to fill in as many of these parameters as you think you need.